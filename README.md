## Orginizational Repo for Google Summer of Code initiatives

In this repo you can find issues and documentation related to LSF participation in initiatives summer of code initiatives such as Google Summer of Code GSoC, Google Season of Docs GSoD, and ESA Summer of Code of Code In Space.
More info can be found on the [repo's wiki](https://gitlab.com/librespacefoundation/soc-org/-/wikis/home).
