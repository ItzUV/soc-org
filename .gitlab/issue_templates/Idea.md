### Sample Idea name.

**Aim**  Please describe the scope of the idea at hand.

**Skills/Knowledge required**
 * p.e. Python
 * p.e. GNU Radio flowgraphs

**Expected results** What should the finalized contribution is expected to do

**Potential mentor(s)** you know who you are

**Related repositories** add if applicable
