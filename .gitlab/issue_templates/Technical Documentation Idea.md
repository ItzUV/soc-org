### Sample Technical Documentation Idea name.

**Aim**  Please describe the scope of the idea at hand.

**Skills/Knowledge required**
 * p.e. Video making
 * p.e. ReadTheDocs familiarity
 * p.e. OHAI familiarity

**Estimated Duration** 'Short-term' OR 'Long-term'

**Expected results** What should the finalized contribution is expected to do

**Potential mentor(s)** If you are unsure about who would mentor the project, leave blank

**Related repositories** add if applicable
